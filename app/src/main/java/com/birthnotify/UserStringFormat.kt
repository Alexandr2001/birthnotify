package com.birthnotify

import com.birthnotify.domain.pojo.YEAR_EMPTY

fun fioToString(surname: String, name: String, patronymic: String?) =
        String.format("%s %s %s", surname, name, patronymic ?: "")

fun dateToString(day: Int, month: Int, year: Int?) =
        String.format("%02d.%02d${if (year != YEAR_EMPTY) ".%d" else ""}", day, month + 1, year)