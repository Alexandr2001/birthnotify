package com.birthnotify.domain.filter

import android.widget.Filter
import android.widget.Filterable
import com.birthnotify.dateToString
import com.birthnotify.domain.pojo.User
import com.birthnotify.fioToString

class Filtering(private val listUsers: MutableList<User>, val callback: (MutableList<User>) -> Unit) : Filterable {
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSequence = constraint.toString()
                val listFiltered: MutableList<User> = if (charSequence.isEmpty()) {
                    listUsers
                } else {
                    val filteredList = mutableListOf<User>()
                    listUsers.forEach {
                        val fio = fioToString(it.surname, it.name, it.patronymic)
                        val date = dateToString(it.day, it.month, it.year)

                        if (fio.contains(charSequence, true) || date.contains(charSequence, true)) {
                            filteredList.add(it)
                        }
                    }
                    filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = listFiltered
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                callback(results?.values as MutableList<User>)
            }
        }
    }
}