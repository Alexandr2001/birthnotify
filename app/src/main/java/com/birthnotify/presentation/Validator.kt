package com.birthnotify.presentation

import android.content.Context
import android.widget.EditText
import com.birthnotify.R

fun validation(context: Context, editText: EditText): Boolean =
        if (editText.text.isNullOrEmpty()) {
            editText.error = context.getString(R.string.error_empty_fields_fio)
            false
        } else {
            true
        }
